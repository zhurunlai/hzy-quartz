# hzy-quartz

#### 介绍

    开箱即用 [Quartz]，WebApi 任务调度中心、统一化、自动化、可视化、管理企业项目中的定时任务。

#### 软件架构

1、后端：.Net5 、 Quartz 

2、前端：Vue3.x 、 Antdv


#### 使用说明

文档：https://gitee.com/hzy6/hzy-quartz/wikis/pages


#### 参与贡献

1、https://gitee.com/liluo_wu

#### 特技

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/173251_6c7946a4_1242080.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/173223_ccec7663_1242080.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/094542_d524f99d_1242080.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/094428_d79f6ab6_1242080.png "屏幕截图.png")
