import axios from "axios";
import tools from './tools'
import router from '@/router/index'

//axios 基本配置
axios.defaults.timeout = 100 * 1000;
axios.defaults.baseURL = process.env.NODE_ENV == "production" ? "/api/" : "http://localhost:5200/api/"; //http://localhost:6789

/* axios.defaults.timeout = 100 * 1000;
axios.defaults.baseURL = appConsts.domainName + "/admin"; //http://localhost:6789 */
let isLoading = true;

//http request 拦截器
axios.interceptors.request.use(
    (config) => {
        if (isLoading) {
            tools.loadingStart();
        }

        let authorization = tools.getAuthorization();
        if (authorization) {
            config.headers["Authorization"] = authorization;
        }
        config.headers["X-Requested-With"] = "XMLHttpRequest";
        // config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        config.headers["Content-Type"] = "application/json; charset=UTF-8";

        if (!config.data) return config;

        if (config.data.isUpload) config.headers["Content-Type"] = "multipart/form-data";
        // else
        //config.data = qs.stringify(config.data); //如果是非上传类型 则 将数据重新组装

        return config;
    },
    (error) => {
        console.log(error);
        return Promise.reject(error);
    }
);

//http response 拦截器
axios.interceptors.response.use(
    (response) => {
        tools.loadingStop();
        const data = response.data;

        if (Object.prototype.hasOwnProperty.call(data, "code")) {
            //     程序异常 = -2,
            // 未授权 = -1,
            // 失败 = 0,
            // 成功 = 1,

            if (data.code === -1) {
                //接口授权码无效
                tools.message(data.message + ",请重新登录授权!", "警告");
                return router.push("/login");
            }
            if (data.code === -2) {
                //服务端异常
                tools.message(data.message, "错误");
                return;
            }
            if (data.code === 0) {
                //失败
                tools.message(data.message, "警告");
                return;
            }
        }

        return response;
    },
    (error) => {
        tools.loadingStop();
        console.log(error);
        if (error.message === 'Network Error') {
            return tools.message('网络连接错误!', "错误");
        }
        if (!error.response) {
            return router.push("/login");
        }
        if (error.response.code === 401) {
            tools.notice("未授权!", "错误");
            return router.push("/login");
        } else {
            return Promise.reject(error);
        }
    }
);

/**
 * 封装get方法
 * @param url
 * @param data
 * @param loading 是否有加载效果
 * @param headers 头部信息
 * @returns {Promise}
 */
export function get(url, data = {}, config = {}) {
    config["params"] = data;
    return new Promise((resolve, reject) => {
        axios
            .get(url, config)
            .then((response) => {
                if (response) resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @param loading 是否有加载效果
 * @param config config信息
 * @returns {Promise}
 */
export function post(url, data = {}, config = {}) {
    return new Promise((resolve, reject) => {
        axios.post(url, data, config).then(
            (response) => {
                if (response) resolve(response.data);
            },
            (err) => {
                reject(err);
            }
        );
    });
}
/**
 * 封装delete请求
 * @param url
 * @param data
 * @param loading 是否有加载效果
 * @param config config信息
 * @returns {Promise}
 */
export function deleteForm(url, config = {}) {
    return new Promise((resolve, reject) => {
        axios.delete(url, config).then(
            (response) => {
                if (response) resolve(response.data);
            },
            (err) => {
                reject(err);
            }
        );
    });
}