/* import tools from "./tools"; */
import { get, post, deleteForm } from './request';


export default {
    /**
     * 查询列表
     * 
     */
    findList(controllerName, projectId) {
        return get(`${controllerName}/find-list/${(projectId ? projectId : '')}`, null);
    },
    /**
     * 删除数据
     * 
     * @param {要删除的id 数组} ids 
     */
    deleteList(controllerName, ids) {
        /* if (ids && ids.length === 0) {
            return tools.message("请选择要删除的数据!", "警告");
        } */
        return deleteForm(`${controllerName}/delete/${ids}`);
    },
    /**
     * 获取表单数据
     * 
     * @param {*} id 
     */
    findForm(controllerName, id) {
        return get(`${controllerName}/find${(id ? '/' + id : '')}`);
    },
    /**
     * 保存表单
     * 
     * @param {表单数据} form 
     */
    saveForm(controllerName, form) {
        return post(`${controllerName}/save`, form);
    },
    /**
     * 运行任务
     *  
     */
    run(controllerName, ids) {
        return post(`${controllerName}/run`, ids);
    },
    /**
     * 关闭任务
     * 
     */
    close(controllerName, ids) {
        return post(`${controllerName}/close`, ids);
    },

    /* 口令请求 */
    check(controllerName, param) {
        return post(`${controllerName}/check/` + param);
    },
};