import { createVNode } from 'vue';
import { message, Modal, notification } from 'ant-design-vue';
import { ExclamationCircleOutlined } from "@ant-design/icons-vue";
import NProgress from 'nprogress';

const tokenKey = "Authorization_Task";
let tools = {
    nprogressState: false,

    /**
     * 加载效果开始
     */
    loadingStart() {
        let value = 0.1;
        const time = setInterval(() => {
            if (this.nprogressState) {
                NProgress.set(1);
                return clearInterval(time);
            }
            value = value + 0.1;
            if (value >= 1) {
                NProgress.set(1);
                return clearInterval(time);
            }
            NProgress.set(value);
        }, 50);
    },
    /**
     * 加载效果结束
     */
    loadingStop() {
        this.nprogressState = true;
    },
    //消息提醒
    message(text, type = '') {
        if (type === '成功') {
            message.success(text);
        } else if (type === '警告') {
            message.warning(text);
        } else if (type === '错误') {
            message.error(text);
        } else {
            message.info(text);
        }
    },
    //alert
    alert(text, type, call) {
        if (type === '成功') {
            Modal.success({
                title: "提醒",
                content: text,
                onOk() {
                    if (call) call();
                }
            });
        } else if (type === '警告') {
            Modal.warning({
                title: "提醒",
                content: text,
                onOk() {
                    if (call) call();
                }
            });
        } else if (type === '错误') {
            Modal.error({
                title: "提醒",
                content: text,
                onOk() {
                    if (call) call();
                }
            });
        } else {
            Modal.info({
                title: "提醒",
                content: text,
                onOk() {
                    console.log('call', call);
                    if (call) call();
                }
            });
        }
    },
    //询问
    confirm(text, successCallBack, cancelCallBack, title = '警告') {
        Modal.confirm({
            title: title,
            content: text,
            okText: '确认',
            cancelText: '取消',
            icon: createVNode(ExclamationCircleOutlined),
            onOk() {
                if (successCallBack) successCallBack();
            },
            onCancel() {
                if (cancelCallBack) cancelCallBack();
            },
        });
    },
    //通知
    notice(text, type, title = "提示") {
        if (type === '成功') {
            notification.success({
                message: title,
                description: text
            });
        } else if (type === '警告') {
            notification.warning({
                message: title,
                description: text
            });
        } else if (type === '错误') {
            notification.error({
                message: title,
                description: text
            });
        } else {
            notification.info({
                message: title,
                description: text
            });
        }
    },
    /**
     * @param {Object} json
     * @param {Object} type： 默认不传 ==>全部小写;传1 ==>全部大写;传2 ==>首字母大写
     * 将json的key值进行大小写转换
     */
    jsonKeysToCase(json, type) {
        if (typeof json == 'object') {
            var tempJson = JSON.parse(JSON.stringify(json));
            toCase(tempJson);
            return tempJson;
        } else {
            return json;
        }

        function toCase(json) {
            if (typeof json == 'object') {
                if (Array.isArray(json)) {
                    json.forEach(function (item) {
                        toCase(item);
                    })
                } else {
                    for (var key in json) {
                        var item = json[key];
                        if (typeof item == 'object') {
                            toCase(item);
                        }
                        delete (json[key]);
                        switch (type) {
                            case 1:
                                //key值全部大写
                                json[key.toLocaleUpperCase()] = item;
                                break;
                            case 2:
                                //key值首字母大写，其余小写
                                json[key.substring(0, 1).toLocaleUpperCase() + key.substring(1).toLocaleLowerCase()] = item;
                                break;
                            default:
                                //默认key值全部小写
                                json[key.toLocaleLowerCase()] = item;
                                break;
                        }
                    }
                }
            }
        }
    },

    //保存 token
    setAuthorization(token) {
        this.setCookie(tokenKey, token);
        //localStorage.setItem(tokenKey, token);
    },
    //获取 token
    getAuthorization() {
        return this.getCookie(tokenKey);
        //return localStorage.getItem(tokenKey);
    },
    //删除 token
    delAuthorization() {
        this.delCookie(tokenKey);
    },
    getCookie: function (name) {
        let reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
        let arr = top.document.cookie.match(reg);
        if (arr)
            return unescape(arr[2]);
        else
            return null;
    },
    delCookie: function (name) {
        var exp = new Date();
        exp.setTime(exp.getTime() - 1);
        var cval = this.getCookie(name);
        if (cval != null)
            top.document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
    },
    //这是有设定过期时间的使用示例：
    //s20是代表20秒
    //h是指小时，如12小时则是：h12
    //d是天数，30天则：d30
    setCookie: function (name, value, time = 'h12', path = '/') {
        if (!time) time = 'h12';
        var strsec = this.getSec(time);
        var exp = new Date();
        exp.setTime(exp.getTime() + strsec * 1);
        top.document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString() + (path ? (";path=" + path) : ";path=/");
    },
    getSec: function (str) {
        var str1 = str.substring(1, str.length) * 1;
        var str2 = str.substring(0, 1);
        if (str2 == "s") {
            return str1 * 1000;
        } else if (str2 == "h") {
            return str1 * 60 * 60 * 1000;
        } else if (str2 == "d") {
            return str1 * 24 * 60 * 60 * 1000;
        }
    },
};

export default tools;