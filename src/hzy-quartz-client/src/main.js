import { createApp } from 'vue'
import App from './App.vue'
//ant-design-vue
import antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
//样式-nprogress
import 'nprogress/nprogress.css';
//自定义全局样式
import '@/assets/styles/index.less';
//路由
import router from '@/router/index';

//启动
createApp(App)
    .use(antd)
    .use(router)
    .mount('#app')