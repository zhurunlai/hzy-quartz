import { createRouter, createWebHashHistory } from 'vue-router';
import home from "@/views/Home";

const routerHistory = createWebHashHistory();

const routes = [
    {
        path: '/',
        component: home,
        redirect: '/project',
    },
    {
        path: '/login',
        component: () => import("@/views/login/index")
    },
    {
        path: '/',
        component: home,
        children: [
            // { path: "/login", component: () => import("@/views/login/index") },
            { path: "/project", component: () => import("@/views/project"), name: 'project' },
            { path: "/task", component: () => import("@/views/task"), name: 'task' },
            { path: "/doc", component: () => import("@/views/doc/index"), name: 'doc' },
        ]
    }
];

const router = createRouter({
    history: routerHistory,
    routes // routes: routes 的简写
})

export default router;