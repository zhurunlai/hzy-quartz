﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Common
{
    public class AppConsts
    {
        public const string TokenKeyName = "TokenKey";

        public const string TokenValueName = "TokenValue";

        public const string UnAuth = "验证未通过,请重新授权！";

        public const string JwtKeyName = "JwtKeyName";

        public const string JwtSecurityKey = "JwtSecurityKey";
    }
}
