﻿using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class QuartzUtil
    {
        /// <summary>
        /// 验证 Cron 表达式是否有效
        /// </summary>
        /// <param name="cronExpression"></param>
        /// <returns></returns>
        public static bool IsValidExpression(this string cronExpression)
        {
            try
            {
                var trigger = new CronTriggerImpl();
                trigger.CronExpressionString = cronExpression;
                var date = trigger.ComputeFirstFireTimeUtc(null);
                return date != null;
            }
            catch //(Exception e)
            {
                return false;
            }

        }



    }
}
