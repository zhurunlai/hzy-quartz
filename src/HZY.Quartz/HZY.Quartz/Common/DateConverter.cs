﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Common
{
    public class DateConverter : IsoDateTimeConverter
    {
        /// <summary>
        /// 类构造
        /// </summary>
        public DateConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }

    }
}
