using HZY.Quartz.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Threading.Tasks;

namespace HZY.Quartz
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

            var host = CreateHostBuilder(args).Build();

            try
            {
                //设置NLog
                LogUtil.Init(logger);
                logger.Debug("初始化 Main !");

                await host.RunAsync();
                //host.Run();
            }
            catch (Exception exception)
            {
                if (!(exception is MessageBox))
                {
                    //NLog: catch setup errors
                    logger.Error(exception, "由于异常而停止程序!");
                }
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    #region Kestrel
                    webBuilder.ConfigureKestrel(options =>
                    {
                        //监听端口 5001 用于 Web Service
                        options.ListenAnyIP(5200, configure => configure.Protocols = HttpProtocols.Http1);
                        //监听端口 5001 用于 Web Service
                        //options.ListenAnyIP(5201, configure => configure.Protocols = HttpProtocols.Http2);

                    });
                    #endregion

                    webBuilder.UseStartup<Startup>()
                    .ConfigureLogging(logging =>
                    {
                        logging.ClearProviders();
                        logging.SetMinimumLevel(LogLevel.Trace);
                    })
                    .UseNLog();  // NLog: Setup NLog for Dependency injection;
                })
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<Worker>();
            })
            .UseWindowsService()
            ;
    }
}
