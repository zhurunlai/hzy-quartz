﻿using AutoMapper;
using HZY.Quartz.Dto;
using HZY.Quartz.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZY.Quartz.Config
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Tasks, TasksProjectDto>();
            CreateMap<Tasks, Tasks>();
            CreateMap<Project, Project>();
            
        }

    }
}
