﻿using HZY.Quartz.Entitys;
using HZY.Quartz.Filter;
using HZY.Quartz.Model;
using HZY.Quartz.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Controllers
{
    /// <summary>
    /// 定时任务控制器
    /// </summary>
    [Authorize]
    [AppAuthorization]
    public class TasksController : AppBaseController<ITaskService>
    {
        public TasksController(ITaskService service) : base(service)
        {

        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("find-list/{projectId?}")]
        public async Task<ApiResult> FindListAsync([FromRoute] Guid? projectId)
            => this.ResultOk(await _service.FindListAsync(projectId));

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("save")]
        public async Task<ApiResult> SaveAsync(Tasks form)
            => this.ResultOk(await _service.SaveAsync(form));

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public async Task<ApiResult> DeleteAsync(Guid id)
            => this.ResultOk(await _service.DeleteAsync(id));

        /// <summary>
        /// 根据Id 查询表单数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("find/{id}")]
        public async Task<ApiResult> FindByIdAsync([FromRoute] Guid id)
            => this.ResultOk(await _service.FindByIdAsync(id));

        /// <summary>
        /// 根据任务id 运行任务调度
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("run")]
        public async Task<ApiResult> RunAsync([FromBody] List<Guid> ids)
        {
            foreach (var item in ids)
            {
                await _service.RunByIdAsync(item);
            }
            return this.ResultOk(true);
        }

        /// <summary>
        /// 根据任务id 关闭任务调度
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("close")]
        public async Task<ApiResult> CloseAsync([FromBody] List<Guid> ids)
        {
            foreach (var item in ids)
            {
                await _service.CloseByIdAsync(item);
            }
            return this.ResultOk(true);
        }


    }
}
