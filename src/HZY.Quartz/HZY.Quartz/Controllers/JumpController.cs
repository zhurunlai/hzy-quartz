﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class JumpController : Controller
    {
        [Route("")]
        public IActionResult Index()
        {
            return Redirect("/client/index.html");
        }
    }
}
