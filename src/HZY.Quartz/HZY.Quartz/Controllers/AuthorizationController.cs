﻿using HZY.Quartz.Common;
using HZY.Quartz.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Controllers
{
    public class AuthorizationController : AppBaseController
    {
        private readonly IConfiguration _configuration;

        public AuthorizationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 检查 口令
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost("check/{password}")]
        public async Task<ApiResult> CheckAsync(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                return this.ResultWarn("请输入口令!");
            }

            var tokenKey = _configuration[AppConsts.TokenKeyName];
            var tokenValue = _configuration[AppConsts.TokenValueName];
            var jwtKeyName = _configuration[AppConsts.JwtKeyName];
            var jwtSecurityKey = _configuration[AppConsts.JwtSecurityKey];

            if (tokenValue != password)
            {
                return this.ResultWarn("口令错误!");
            }

            var token = JwtTokenUtil.CreateToken(tokenValue, jwtSecurityKey, jwtKeyName);

            const string tokenType = "Bearer ";

            return await Task.FromResult(this.ResultOk(new
            {
                tokenKey,
                token = tokenType + token
            }));
        }


    }
}
