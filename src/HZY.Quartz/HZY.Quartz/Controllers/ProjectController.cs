﻿using HZY.Quartz.Entitys;
using HZY.Quartz.Filter;
using HZY.Quartz.Model;
using HZY.Quartz.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Controllers
{
    /// <summary>
    /// 项目控制器
    /// </summary>
    [Authorize]
    [AppAuthorization]
    public class ProjectController : AppBaseController<IProjectService>
    {

        public ProjectController(IProjectService service) : base(service)
        {

        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("find-list")]
        public async Task<ApiResult> FindListAsync()
            => this.ResultOk(await _service.FindListAsync());

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("save")]
        public async Task<ApiResult> SaveAsync(Project form)
            => this.ResultOk(await _service.SaveAsync(form));

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public async Task<ApiResult> DeleteAsync(Guid id)
            => this.ResultOk(await _service.DeleteAsync(id));

        /// <summary>
        /// 根据id 查询表单数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("find/{id}")]
        public async Task<ApiResult> FindByIdAsync([FromRoute] Guid id)
            => this.ResultOk(await _service.FindByIdAsync(id));











    }
}
