﻿using HZY.Quartz.Filter;
using HZY.Quartz.Model;
using HZY.Quartz.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace HZY.Quartz.Controllers
{
    /// <summary>
    /// 运行日志控制器
    /// </summary>
    [Authorize]
    [AppAuthorization]
    public class JobLoggerController : AppBaseController<IJobLoggerService>
    {
        public JobLoggerController(IJobLoggerService service) : base(service)
        {

        }

        /// <summary>
        /// 获取运行日志
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet("{taskId}/{page}/{size}")]
        public ApiResult GetJobLoggers(Guid taskId, int page, int size)
        {
            var data = _service.FindListById(taskId)
                .OrderByDescending(w => w.CreateTime)
                .Skip((page - 1) * size)
                .Take(size)
                .ToList()
                ;
            return this.ResultOk(data);
        }





    }
}
