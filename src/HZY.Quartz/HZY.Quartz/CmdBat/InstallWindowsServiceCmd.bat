set serviceName=HZY.Quartz
set serviceFilePath=D:\Projects\HZY.Quartz\HZY.Quartz.exe
set serviceDescription=HZY.Quartz 定时任务对象 描述

sc create %serviceName% BinPath=%serviceFilePath%
sc config %serviceName% start=auto
sc description %serviceName% %serviceDescription%
sc start %serviceName%
pause