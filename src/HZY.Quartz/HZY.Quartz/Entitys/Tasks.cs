﻿using HZY.Quartz.Common;
using System;
using System.Text.Json.Serialization;

namespace HZY.Quartz.Entitys
{
    /// <summary>
    /// 定时任务
    /// </summary>
    public class Tasks
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 分组名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 间隔表达式
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// 请求 token 密钥
        /// </summary>
        public string HeaderToken { get; set; }

        /// <summary>
        /// 请求方式
        /// </summary>
        public RequsetModeEnum RequsetMode { get; set; } = RequsetModeEnum.Post;

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 运行状态
        /// </summary>
        public StateEnum State { get; set; } = StateEnum.未运行;

        /// <summary>
        /// 执行时间
        /// </summary>
        public DateTime? ExecuteTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

    }

    /// <summary>
    /// 请求方式
    /// </summary>
    public enum RequsetModeEnum
    {
        Post,
        Get,
        Delete
    }

    /// <summary>
    /// 状态情况
    /// </summary>
    public enum StateEnum
    {
        未运行,
        运行中
    }

}
