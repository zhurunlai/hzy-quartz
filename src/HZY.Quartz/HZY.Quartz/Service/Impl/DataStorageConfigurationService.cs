﻿using HZY.Quartz.Entitys;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Service.Impl
{
    /// <summary>
    /// 数据配置
    /// </summary>
    public class DataStorageConfigurationService : IDataStorageConfigurationService
    {
        private readonly IConfiguration _configuration;

        public DataStorageConfigurationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public virtual bool IsUseRedis()
        {
            var useRedis = _configuration["UseRedis"];

            if (string.IsNullOrWhiteSpace(useRedis))
            {
                throw new Exception("项目启动未发现配置项：UseRedis");
            }

            return useRedis == "1";
        }

        public virtual string GetProjectPathOrKey()
        {
            return _configuration[$"{(this.IsUseRedis() ? "Redis" : "")}DbPath:Project"];
        }

        public virtual string GetTasksPathOrKey()
        {
            return _configuration[$"{(this.IsUseRedis() ? "Redis" : "")}DbPath:Tasks"];
        }

        public virtual string GetRedisConnectionString()
        {
            return _configuration["RedisConnectionString"];
        }

    }
}
