﻿using HZY.Quartz.Common.Redis;
using HZY.Quartz.Entitys;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Service.Impl
{
    /// <summary>
    /// Job 运行 日志
    /// </summary>
    public class JobLoggerService : IJobLoggerService
    {
        private ConcurrentBag<JobLoggerInfo> jobLoggerInfos;
        private string JobLoggerKey = "HC.Quartz:JobLogger";
        private long ListMaxValue = 999;//集合最大值
        private readonly IDataStorageConfigurationService _dataStorageConfigurationService;
        private readonly IRedisService _redisService;

        public JobLoggerService(IDataStorageConfigurationService dataStorageConfigurationService, IRedisService redisService)
        {
            if (!dataStorageConfigurationService.IsUseRedis())
            {
                jobLoggerInfos ??= new ConcurrentBag<JobLoggerInfo>();
            }

            _dataStorageConfigurationService = dataStorageConfigurationService;
            _redisService = redisService;
        }

        private IEnumerable<JobLoggerInfo> FindAll()
        {
            //if (_dataStorageConfigurationService.IsUseRedis())
            //{
            //    var json = _redisService.Database.StringGet(JobLoggerKey);
            //    return string.IsNullOrWhiteSpace(json) ? new ConcurrentBag<JobLoggerInfo>() : JsonConvert.DeserializeObject<List<JobLoggerInfo>>(json);
            //}

            return jobLoggerInfos ?? new ConcurrentBag<JobLoggerInfo>();
        }

        public IEnumerable<JobLoggerInfo> FindListById(Guid tasksId)
        {
            if (tasksId == Guid.Empty) return new ConcurrentBag<JobLoggerInfo>();

            if (_dataStorageConfigurationService.IsUseRedis())
            {
                var json = _redisService.Database.StringGet($"{JobLoggerKey}:{tasksId}");
                return string.IsNullOrWhiteSpace(json) ? new List<JobLoggerInfo>() : JsonConvert.DeserializeObject<List<JobLoggerInfo>>(json);
            }

            return FindAll().Where(w => w.TasksId == tasksId);
        }

        public void Write(JobLoggerInfo jobLoggerInfo)
        {
            if (jobLoggerInfo == null) return;
            var tasksId = jobLoggerInfo?.TasksId ?? Guid.Empty;

            if (_dataStorageConfigurationService.IsUseRedis())
            {
                var list = this.FindListById(tasksId)?.ToList() ?? new List<JobLoggerInfo>();
                if (list.Count > ListMaxValue)
                {
                    list.Clear();
                    list ??= new List<JobLoggerInfo>();
                }

                list.Add(jobLoggerInfo);

                _redisService.Database.StringSet($"{JobLoggerKey}:{tasksId}", JsonConvert.SerializeObject(list));
            }
            else
            {
                jobLoggerInfos ??= new ConcurrentBag<JobLoggerInfo>();
                jobLoggerInfos.Add(jobLoggerInfo);

                if (jobLoggerInfos.Count > ListMaxValue)
                {
                    jobLoggerInfos.Clear();
                    jobLoggerInfos ??= new ConcurrentBag<JobLoggerInfo>();
                }
            }
        }





    }
}
