﻿using HZY.Quartz.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Service
{
    /// <summary>
    /// 项目管理 服务
    /// </summary>
    public interface IProjectService
    {
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Project>> FindListAsync();

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        Task<Project> SaveAsync(Project form);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteAsync(Guid id);

        /// <summary>
        /// 根据 Id 查询表单数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Project> FindByIdAsync(Guid id);

    }
}
