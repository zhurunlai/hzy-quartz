﻿using HZY.Quartz.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Service
{
    public interface IApiRequestService
    {

        Task<(bool IsSuccess, string Message)> RequestAsync(RequsetModeEnum requsetMode, string apiUrl, string headerKeyValue);



    }
}
