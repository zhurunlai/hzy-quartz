﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HZY.Quartz.Service
{
    public interface IDataStorageConfigurationService
    {
        /// <summary>
        /// 是否使用Redis
        /// </summary>
        /// <returns></returns>
        public bool IsUseRedis();

        /// <summary>
        /// 获取项目的 文件地址 或者 项目 redis key
        /// </summary>
        /// <returns></returns>
        public string GetProjectPathOrKey();

        /// <summary>
        /// 获取任务的 文件地址 或者 项目 redis key
        /// </summary>
        /// <returns></returns>
        public string GetTasksPathOrKey();

        /// <summary>
        /// 获取 Redis 连接字符串
        /// </summary>
        /// <returns></returns>
        public string GetRedisConnectionString();

    }
}
