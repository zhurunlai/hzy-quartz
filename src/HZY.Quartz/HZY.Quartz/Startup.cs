using HZY.Quartz.Common.Redis;
using HZY.Quartz.Service.Impl;
using HZY.Quartz.Service.Jobs;
using HZY.Quartz.Service;
using HZY.Quartz.Config;
using HZY.Quartz.Filter;
using HZY.Quartz.Service;
using HZY.Quartz.Service.Impl;
using HZY.Quartz.Service.Jobs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.Impl;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.IO;
using System.Text;

namespace HZY.Quartz
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jwtKeyName = Configuration["JwtKeyName"];
            var jwtSecurityKey = Configuration["JwtSecurityKey"];

            services.AddControllers(options =>
            {
                options.Filters.Add<ApiExceptionFilter>();
            })
            ;

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HZY.Quartz", Version = "v1" });

                //为 Swagger JSON and UI设置xml文档注释路径
                var xmlPath = Path.Combine(AppContext.BaseDirectory, "HZY.Quartz.xml");
                c.IncludeXmlComments(xmlPath, true);

                #region Jwt token 配置

                //option.OperationFilter<AppService.SwaggerParameterFilter>(); // 给每个接口配置授权码传入参数文本框
                //
                c.OperationFilter<AddResponseHeadersFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                //很重要！这里配置安全校验，和之前的版本不一样
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                //开启 oauth2 安全描述
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "JWT授权(数据将在请求头中进行传输) 直接在下框中输入Bearer {token}（注意两者之间是一个空格）\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    //Scheme = "basic",
                });

                #endregion

            });

            services.AddCors(options =>
            {
                options.AddPolicy("HZYCors", builder =>
                {
                    builder.WithOrigins("*")
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    //.AllowAnyOrigin()
                    //.AllowCredentials();
                    //6877
                });
            });

            #region JWT

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true, //是否验证Issuer
                        ValidateAudience = true, //是否验证Audience
                        ValidateLifetime = true, //是否验证失效时间
                        ValidateIssuerSigningKey = true, //是否验证SecurityKey
                        ValidAudience = jwtKeyName, //Audience
                        ValidIssuer = jwtKeyName, //Issuer，这两项和前面签发jwt的设置一致
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecurityKey)) //拿到SecurityKey
                    };
                });

            #endregion

            #region 实例注册
            //AutoMapper 配置
            services.AddAutoMapper(typeof(AutoMapperConfig));
            //文件数据服务
            services.AddTransient<IDataService, DataService>();
            //项目服务
            services.AddTransient<IProjectService, ProjectService>();
            //任务服务
            services.AddTransient<ITaskService, TaskService>();
            //定时任务 服务
            services.AddTransient<IQuartzJobService, QuartzJobService>();
            //注册ISchedulerFactory的实例。
            services.AddTransient<ISchedulerFactory, StdSchedulerFactory>();
            //web api 请求服务
            services.AddTransient<IApiRequestService, ApiRequestService>();
            //Job 实例化工厂
            services.AddSingleton<ResultfulApiJobFactory>();
            //Reultful 风格 api 请求 任务
            services.AddTransient<ResultfulApiJob>();
            //任务日志
            services.AddSingleton<IJobLoggerService, JobLoggerService>();
            //项目配置信息
            services.AddSingleton<IDataStorageConfigurationService, DataStorageConfigurationService>();
            //redis 注册
            RepositoryRedisModule.RegisterRedisRepository(services, Configuration["RedisConnectionString"]);
            //services.AddScoped<IRedisService, RedisService>();

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HZY.Quartz v1"));
            }
            app.UseCors("HZYCors");
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            //app.UseAuthorization();

            #region JWT

            app.UseAuthentication();
            app.UseAuthorization();

            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
